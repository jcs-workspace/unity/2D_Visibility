[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)
[![Unity Engine](https://img.shields.io/badge/unity-2023.2.19f1-black.svg?style=flat&logo=unity)](https://unity3d.com/get-unity/download/archive)

# 2D_Visibility

Implementation of [2D Visibility][2d Visibility from Red Blob Games] in [Unity][] using [C#][].

This project uses 2D components with the [Universal Rendering Pipeline (URP)][URP].

## 🏆 Features

All references I've encountered only support AABB box visibility. I wanted a
more flexible solution that accommodates various shapes. Thus, this
experimental project was developed. Please note, this project is not intended to
be a plugin or library, so it cannot be used directly. It serves solely for
educational purposes and experiments with 2D visibility features.


This project demonstrates four shapes: triangle, rectangle (square), circle,
and hexagon. They all use the same component, [PolygonCollider2D][], so in
theory, it should work with any shape!

## 🖼 Screenshots

| ![](etc/demo.gif) | ![](etc/1.png) |
|---|---|
| ![](etc/2.png) |  |

## 🎮 Control

- <kbd>W</kbd>, <kbd>A</kbd>, <kbd>S</kbd>, <kbd>E</kbd> to control
- <kbd>Mouse</kbd> to rotate
- <kbd>E</kbd> to toggle flash light

## 🔗 References

- [2d Visibility from Red Blob Games][]
- [Shadow Casting: 2D Game Development Part 2 in Unity engine.](https://www.youtube.com/watch?v=wUDqSXsTY2g)


<!-- Links -->

[2d Visibility from Red Blob Games]: https://www.redblobgames.com/articles/visibility/
[Unity]: https://unity.com/
[C#]: https://en.wikipedia.org/wiki/C_Sharp_(programming_language)

[URP]: https://unity.com/srp/universal-render-pipeline
[PolygonCollider2D]: https://docs.unity3d.com/ScriptReference/PolygonCollider2D.html
