using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace Gameplay
{
    [RequireComponent(typeof(PolygonCollider2D))]
    public class Wall : MonoBehaviour
    {
        /* Variables */

        private PolygonCollider2D mCollider = null;

        [Tooltip("Used to draw GL.")]
        [SerializeField]
        private Material mGLDraw = null;

        [Tooltip("Shadow color")]
        [SerializeField]
        private Color mColor = Color.black;

        [Tooltip("How far does the shadow covers.")]
        [SerializeField]
        [Range(0.0f, 30000.0f)]
        private float mProjectionLength = 100.0f;

        /* Getter & Setter */

        /* Functions */

        private void Awake()
        {
            mCollider = this.GetComponent<PolygonCollider2D>();
        }

        private void OnEnable()
        {
            RenderPipelineManager.endCameraRendering += RenderPipelineManager_endCameraRendering;
        }

        private void OnDisable()
        {
            RenderPipelineManager.endCameraRendering -= RenderPipelineManager_endCameraRendering;
        }

        private void RenderPipelineManager_endCameraRendering(ScriptableRenderContext context, Camera camera)
        {
            OnPostRender();
        }

        private void OnPostRender()
        {
            if (mGLDraw == null)
            {
                Debug.Log("Missing GLDraw");
                return;
            }

            GL.PushMatrix();
            mGLDraw.SetPass(0);
            GL.Begin(GL.TRIANGLES);
            GL.Color(mColor);

            int len = mCollider.points.Length;

            var views = Player.instance.Views;

            foreach (var view in views)
            {
                Vector2 from = view.transform.position;

                for (int index = 0; index < len; ++index)
                {
                    // NOTE: The points is sorted in clockwise; therefore, we can simply
                    // just wrapped around.
                    int nextIndex = index + 1;
                    int prevIdex = index - 1;
                    if (prevIdex < 0) prevIdex = len - 1;
                    if (nextIndex >= len) nextIndex = 0;

                    var current = mCollider.points[index];   // current point
                    var prev = mCollider.points[prevIdex];   // pre sibling
                    var next = mCollider.points[nextIndex];  // next sibling

                    // Handle rotation and scale.
                    Vector2 currentVertex = mCollider.transform.TransformPoint(current);
                    Vector2 prevVertex = mCollider.transform.TransformPoint(prev);
                    Vector2 nextVertex = mCollider.transform.TransformPoint(next);

                    DrawShadow(from, currentVertex, prevVertex, nextVertex);
                }

            }

            GL.End();
            GL.PopMatrix();
        }

        private void DrawShadow(Vector2 from, Vector2 to, Vector2 prev, Vector2 next)
        {
            Vector2 proj = (to - from).normalized * mProjectionLength;
            Vector2 projPrev = (prev - from).normalized * mProjectionLength;
            Vector2 projNext = (next - from).normalized * mProjectionLength;

            GL.Vertex(new Vector3(to.x, to.y));
            GL.Vertex(new Vector3(prev.x, prev.y));
            GL.Vertex(new Vector3(proj.x, proj.y));

            GL.Vertex(new Vector3(to.x, to.y));
            GL.Vertex(new Vector3(projPrev.x, projPrev.y));
            GL.Vertex(new Vector3(projNext.x, projNext.y));
        }
    }
}
