using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Gameplay
{
    [RequireComponent(typeof(Light2D))]
    public class FlashLight : MonoBehaviour
    {
        /* Variables */

        private Light2D mLight = null;

        private bool mToggle = false;

        /* Getter & Setter */

        /* Functions */

        private void Awake()
        {
            this.mLight = this.GetComponent<Light2D>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                if (mToggle)
                {
                    mLight.pointLightOuterAngle = 360;
                    mLight.pointLightInnerAngle = 360;
                }
                else
                {
                    mLight.pointLightOuterAngle = 120;
                    mLight.pointLightInnerAngle = 60;
                }

                mToggle = !mToggle;
            }
        }
    }
}
