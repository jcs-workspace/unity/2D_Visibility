using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class Player : MonoBehaviour
    {
        /* Variables */

        public static Player instance = null;

        [Tooltip("")]
        [SerializeField]
        private List<View> mViews = null;

        [SerializeField]
        private PolygonCollider2D mCollider = null;

        [SerializeField]
        private List<Color> mGizmosColors = null;

        private Vector3 mVelocity = Vector3.zero;

        [Tooltip("Speed.")]
        [SerializeField]
        [Range(0.01f, 10.0f)]
        private float mSpeed = 5.0f;

        /* Getter & Setter */

        public List<View> Views { get { return mViews; } }

        /* Functions */

        private void Awake()
        {
            instance = this;
        }

        private void Update()
        {
            DoLook();
            DoMovement();
        }

        private void DoLook()
        {
            var dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        private void DoMovement()
        {
            this.mVelocity = Vector3.zero;

            if (Input.GetKey(KeyCode.W))
            {
                this.mVelocity.y = mSpeed;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                this.mVelocity.y = -mSpeed;
            }

            if (Input.GetKey(KeyCode.A))
            {
                this.mVelocity.x = -mSpeed;
            }
            else if (Input.GetKey(KeyCode.D))
            {
                this.mVelocity.x = mSpeed;
            }

            this.transform.position += mVelocity * Time.deltaTime;
        }

        public void AddView(View view)
        {
            this.mViews.Add(view);
        }

        private void OnDrawGizmos()
        {
            int index = 0;

            foreach (var point in mCollider.points)
            {
                Gizmos.color = mGizmosColors[index];

                Vector2 globalPoint = mCollider.transform.TransformPoint(point);

                Gizmos.DrawLine(globalPoint, this.transform.position);

                ++index;
            }
        }
    }
}
