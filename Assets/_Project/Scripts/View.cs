using UnityEngine;

namespace Gameplay
{
    public class View : MonoBehaviour
    {
        /* Variables */

        /* Getter & Setter */

        /* Functions */

        private void Start()
        {
            Player.instance.AddView(this);
        }
    }
}
